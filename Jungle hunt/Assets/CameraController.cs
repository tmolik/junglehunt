﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    internal PlayerController Player;
    

	// Use this for initialization
	void Start () {
        Player = GameObject.Find("Player").GetComponent<PlayerController>();
	}
	
	// Update is called once per frame
	void Update () {
	
        if(GameController.Get.CurrentGameMode == GameController.GameMode.Lianas)
        {
            transform.position = new Vector3(Player.transform.position.x, transform.position.y, transform.position.z);
        }
        else if (GameController.Get.CurrentGameMode == GameController.GameMode.Water)
        {
            transform.position += Vector3.left * 1.2f* Time.deltaTime;
        }	
	}
}
