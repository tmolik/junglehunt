﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingEnemyController : MonoBehaviour
{


    float randomizer = 1f;
    private bool hasPlayerOnIt = false;
    private GameObject playerGO = null;

    private int direction = 1;

    // Use this for initialization
    void Start()
    {
        randomizer = Random.Range(1, 1.5f);
        if (GetComponent<Animator>() != null)
            GetComponent<Animator>().Play("octopusIdle");
    }

    // Update is called once per frame
    void Update()
    {
        if (hasPlayerOnIt)
        {
            transform.position += Vector3.down * Time.deltaTime * randomizer;
            //if (transform.position.y <= -7)
            //    transform.position = new Vector3(transform.position.x, -7, transform.position.z);
            playerGO.transform.position = transform.position + Vector3.down * 0.3f;
        }
        else
        {
            if (direction == 1 && transform.position.y > 1.1f)
                direction = -1;
            if (direction == -1 && transform.position.y < -7)
                direction = 1;
            transform.position += Vector3.up * direction * Time.deltaTime * randomizer;
        }
    }

    public void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.name == "Player" && coll.GetComponent<PlayerController>().Fighting == false)
        {
            coll.GetComponent<PlayerController>().Fighting = true;
            coll.GetComponent<PlayerController>().FightingGO = gameObject;
            coll.GetComponent<PlayerController>().EnemyType = GameController.WaterEnemies.DeepEnemy;
            hasPlayerOnIt = true;
            playerGO = coll.gameObject;
            GameObject.FindObjectOfType<GameUiController>().SetText("Jesteś wciągany w głębiny!");
            if (GetComponent<Animator>() != null)
                GetComponent<Animator>().Play("octopusHold");
        }
    }
}
