﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;

class GameController : MonoBehaviour
{
    internal static GameController Get;

    public enum GameMode { Lianas, Water, Menu }
    public enum WaterEnemies { Crocodile, DeepEnemy }

    public string currentLevel;
    public Dictionary<string, GameMode> levels = new Dictionary<string, GameMode>();
    public int Points;
    public int LifePoints;

    public GameMode CurrentGameMode;

    internal float changeMapTimer = 0;
    internal bool changingMap = false;

    public AudioClip SongClip;
    public AudioClip JumpClip;
    public AudioClip WinClip;


    public void Start()
    {
        Get = this;
        currentLevel = "Menu";
        CurrentGameMode = GameMode.Menu;
        levels.Add("Menu", GameMode.Menu);
        levels.Add("level1", GameMode.Lianas);
        levels.Add("level3", GameMode.Water);
        levels.Add("level2", GameMode.Lianas);
        levels.Add("level4", GameMode.Water);
        levels.Add("level5", GameMode.Lianas);
        levels.Add("level6", GameMode.Water);
        DontDestroyOnLoad(this.gameObject);
        Points = 0;
        LifePoints = 3;
        GetComponent<AudioSource>().clip = SongClip;
        GetComponent<AudioSource>().Play();
    }

    public void Update()
    {

        if (changingMap)
        {
            float tim = Mathf.Round(2 - changeMapTimer);
            GameObject.FindObjectOfType<GameUiController>().SetText("Zmiana mapy nastąpi za "+ tim);

            changeMapTimer += Time.deltaTime;
            if (changeMapTimer > 2)
            {
                changingMap = false;
                LoadNextLevel();
                GameObject.FindObjectOfType<GameUiController>().SetText("");

            }
        }
        if (CurrentGameMode == GameMode.Menu)
        {
            if (Input.touchCount>0)
            {
                LoadNextLevel();
            }
        }
    }

    public void LoadNextLevel()
    {
        Debug.Log("Probuje zaladowac poziom");
        int index = levels.Keys.ToList().IndexOf(currentLevel);
        if (levels.Count >= index + 2)
        {
            currentLevel = levels.Keys.ToList()[index + 1];
            CurrentGameMode = levels[currentLevel];
            SceneManager.LoadScene(currentLevel);
        }
        else
        {
            Debug.Log("No i koniec");
        }
    }

    public void ResetLevel()
    {
        LifePoints--;
        if (LifePoints == 0)
        {
            LifePoints = 3;
            SceneManager.LoadScene("Menu");
        }
        else
        {
            SceneManager.LoadScene(currentLevel);
        }
    }


}

