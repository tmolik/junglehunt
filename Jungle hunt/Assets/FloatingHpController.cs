﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingHpController : MonoBehaviour {

    float randomizer = 1f;
	// Use this for initialization
	void Start () {
        randomizer = Random.Range(1, 3);
	}
	
	// Update is called once per frame
	void Update () {
        transform.position += Vector3.up * Time.deltaTime * randomizer;
        if (transform.position.y > 1.15f)
            Destroy(gameObject);
	}

    public void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.name == "Player") 
        {
            coll.GetComponent<PlayerController>().HP = Mathf.Min(coll.GetComponent<PlayerController>().HP + 0.07f, 1);
            coll.GetComponent<PlayerController>().airAvailable = Mathf.Min(coll.GetComponent<PlayerController>().airAvailable + 0.05f, 1);
            GameController.Get.Points += 2;
            Destroy(gameObject);

        }
    }
}
