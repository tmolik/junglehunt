﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrocodileController : MonoBehaviour
{

    public GameObject PlayerObject;

    private float randomizer;
    private Vector3 startPos;
    private float timer;
    // Use this for initialization
    void Start()
    {
        PlayerObject = GameObject.Find("Player");
        randomizer = Random.Range(0.4f, 0.8f);
        startPos = transform.position;
        timer = 0;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (Vector3.Distance(transform.position, PlayerObject.transform.position) < 2)
        {
            transform.position = Vector3.MoveTowards(transform.position, PlayerObject.transform.position, Time.deltaTime);
            startPos = transform.position;
            timer = 0;
        }
        else
        {
            transform.position = startPos + Vector3.up * Mathf.Sin(timer) * randomizer;
            if (transform.position.y > 0.6f)
            {
                transform.position = new Vector3(transform.position.x, 0.6f, transform.position.z);
            }
            if (transform.position.y < -6.6f)
            {
                transform.position = new Vector3(transform.position.x, -6.6f, transform.position.z);
            }
        }
    }


    public void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.name == "Player" && coll.GetComponent<PlayerController>().Fighting == false)
        {
            coll.GetComponent<PlayerController>().Fighting = true;
            coll.GetComponent<PlayerController>().FightingGO = gameObject;
            coll.GetComponent<PlayerController>().EnemyType = GameController.WaterEnemies.Crocodile;
            GameObject.FindObjectOfType<GameUiController>().SetText("Dotykaj by pokonać krokodyla");
            GetComponent<Animator>().Play("CrocodileAnim");

        }
    }

    public void OnTriggerExit2D(Collider2D coll)
    {
        if (coll.gameObject.name == "Player")
        {
            coll.GetComponent<PlayerController>().Fighting = false;
            coll.GetComponent<PlayerController>().FightingGO = null;
            GameObject.FindObjectOfType<GameUiController>().SetText("");
        }
    }


}
