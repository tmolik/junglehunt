﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static PlayerController Get { get; set; }

    public GameObject Liana;
    public GameObject LianaChild;

    internal bool jumping = false;
    public bool onLiana = false;
    private bool started = false;
    private Vector3 startPos;
    private Vector3 dest;
    private float timer;
    private float jumpTime = 1f;

    internal float airAvailable = 1f;
    public float HP = 1f;

    internal bool Fighting;
    internal GameObject FightingGO;
    internal GameController.WaterEnemies EnemyType;
    private float fightingCounter;

    private GameObject mainCamera;
    // Use this for initialization
    void Start()
    {
        Get = this;
        mainCamera = GameObject.Find("Main Camera");
        if (GameController.Get.CurrentGameMode == GameController.GameMode.Lianas)
            GetComponent<Animator>().Play("playerIdle");
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y < -8)
        {
            GameController.Get.ResetLevel();
        }
        Camera cam = GameObject.FindObjectOfType<CameraController>().gameObject.GetComponent<Camera>();
        if (GameController.Get.CurrentGameMode == GameController.GameMode.Lianas)
        {  
            /*Skakanie*/
            if (jumping)
            {
                var height = Mathf.Sin(Mathf.PI * timer) * 1;
                transform.position = Vector3.Lerp(startPos, dest, timer) + Vector3.up * height;
                timer += Time.deltaTime / jumpTime;
                if (timer >= 1) jumping = false;
            }
            /*Pobyt na lianie*/
            if (onLiana)
            {
                transform.position = Vector3.Lerp(transform.position, LianaChild.transform.position, Time.deltaTime * 10);
                transform.localRotation = Quaternion.Slerp(transform.rotation, Liana.transform.localRotation, Time.deltaTime * 10);
            }
            /*Spadanie*/
            if (!jumping && !onLiana && started)
            {
                transform.position += Vector3.down * 5 * Time.deltaTime;
                transform.position += Vector3.left * 4 * Time.deltaTime;
                
            }

            /*Podskok*/
            if (Input.touchCount > 0 && !jumping && (onLiana || !started))
            {
                if (Vector2.Distance(cam.ScreenToWorldPoint(Input.GetTouch(0).position), transform.position) < 1.5f)
                {
                    started = true;
                    jumping = true;
                    onLiana = false;
                    timer = 0;
                    startPos = transform.position;
                    dest = transform.position + Vector3.left * 6;
                    GetComponent<Animator>().Play("playerJump");
                    GetComponent<AudioSource>().clip = GameController.Get.JumpClip;
                    GetComponent<AudioSource>().Play();
                }
            }
            float vertical = 0;
            if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Stationary)
                vertical = cam.ScreenToWorldPoint(Input.GetTouch(0).position).y > transform.position.y ? 1 : -1;
            
            /*Poruszanie sie w gore/dol po lianie*/
            if (vertical != 0 && LianaChild != null && onLiana)
            {
                var nextPosition = LianaChild.transform.localPosition + Vector3.up * vertical * Time.deltaTime * 1 / 2;
                if (vertical > 0)
                {
                    if (nextPosition.y <= -0.5)
                        LianaChild.transform.localPosition += Vector3.up * vertical * Time.deltaTime;
                }
                if (vertical < 0)
                {
                    if (nextPosition.y >= -1.25)
                        LianaChild.transform.localPosition += Vector3.up * vertical * Time.deltaTime;
                }

            }

            mainCamera.transform.position = new Vector3(transform.position.x, mainCamera.transform.position.y, mainCamera.transform.position.z);
        }
        else if (GameController.Get.CurrentGameMode == GameController.GameMode.Water)
        {
            if (Fighting && EnemyType == GameController.WaterEnemies.DeepEnemy)
            {/*Jeśli walczymy z wrogiem ktory nas wciaga nie mozemy sie ruszac*/}
            else
            {
                #region Poruszanie
                if (Input.touchCount > 0)// && Input.GetTouch(0).phase == TouchPhase.Stationary)
                {
                    Vector2 touchPos = cam.ScreenToWorldPoint(Input.GetTouch(0).position);
                    
                    var horizontal = touchPos.x > transform.position.x ? 1 : -1;//  Input.GetAxis("Horizontal");
                    var vertical = touchPos.y > transform.position.y ? 1 : -1;// Input.GetAxis("Vertical");
                    Debug.Log("horizontal = " + horizontal + " vertical =" + vertical);
                    float yPos = 0;
                    float xPos = 0;
                    if (vertical > 0)
                    {
                        yPos = Mathf.Min(transform.position.y + 2 * vertical * Time.deltaTime, 1.2f);
                    }
                    else if (vertical < 0)
                    {
                        yPos = Mathf.Max(transform.position.y - 2 * Mathf.Abs(vertical) * Time.deltaTime, -5f);
                    }

                    if (horizontal > 0)
                    {
                        xPos = Mathf.Min(transform.position.x + 1 * horizontal * Time.deltaTime, mainCamera.transform.position.x + 3);
                    }
                    else if (horizontal < 0)
                    {
                        xPos = Mathf.Max(transform.position.x - 3 * Mathf.Abs(horizontal) * Time.deltaTime, mainCamera.transform.position.x - 3);
                    }

                    transform.position = new Vector3(xPos, yPos, transform.position.z);
                }
                else
                {
                    float yPos = transform.position.y;
                    float xPos = Mathf.Min(transform.position.x, mainCamera.transform.position.x + 3);
                    transform.position = new Vector3(xPos, yPos, transform.position.z);

                }
                #endregion
            }
            /*Zdobywanie/tracenie powietrza i tracenie hp jesli nie ma sie powietrza*/
            if (transform.position.y < 1.15)
            {
                airAvailable = Mathf.Max(airAvailable - 0.05f * Time.deltaTime, 0);
            }
            else
            {
                airAvailable = Mathf.Min(airAvailable + 0.2f * Time.deltaTime, 1);
            }
            if (airAvailable <= 0)
                HP = Mathf.Max(HP - 0.06f * Time.deltaTime, 0);


            /*Walka*/
            if (Fighting)
            {
                HP = Mathf.Max(HP - 0.07f * Time.deltaTime, 0);
                if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    fightingCounter++;
                }
                if (fightingCounter >= (EnemyType == GameController.WaterEnemies.Crocodile ?8 : 5))
                {
                    fightingCounter = 0;
                    Destroy(FightingGO);
                    Fighting = false;
                    GameObject.FindObjectOfType<GameUiController>().SetText("");
                    GameController.Get.Points += 4;

                }
            }
            if (HP == 0)
            {
                GameController.Get.ResetLevel();
            }
        }

    }

    public void OnTriggerEnter2D(Collider2D coll)
    {
        if (GameController.Get.CurrentGameMode == GameController.GameMode.Water)
        {
            if (coll.gameObject.name == "finish")
            {
                GetComponent<AudioSource>().clip = GameController.Get.WinClip;
                GetComponent<AudioSource>().Play();
                GameController.Get.changingMap = true;
                GameController.Get.changeMapTimer = 0;
                //GameController.Get.LoadNextLevel();

            }
        }
    }
}
