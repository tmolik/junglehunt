﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerController : MonoBehaviour {

    public GameObject FloatingHpPrefab;
    public GameObject DeepEnemyPrefab;
    public Vector3 StartingPosition;
    public float timer;
    public float helpTimer;
    private float randomizer;
	// Use this for initialization
	void Start () {
        StartingPosition = transform.position;
        timer = 0;
        helpTimer = 2;
        randomizer = Random.Range(1, 3);
	}
	
	// Update is called once per frame
	void Update () {
        if (Vector2.Distance(transform.position, GameObject.Find("Player").transform.position) > 30) return;
        timer += Time.deltaTime;
        transform.position = StartingPosition + Vector3.left * 8f * Mathf.Sin(Time.time*randomizer);
        if (timer > helpTimer)
        {
            timer = 0;
            Spawn();
            helpTimer = Random.Range(2.5f, 4);
        }
	}

    public void Spawn()
    {
        if (Random.Range(0, 100) > 50)
            SpawnHp();
        else
            SpawnEnemy();
    }

    public void SpawnHp()
    {
        GameObject hpGO = Instantiate(FloatingHpPrefab);
        hpGO.transform.position = transform.position;
    }

    public void SpawnEnemy()
    {
        GameObject enemyGO = Instantiate(DeepEnemyPrefab);
        enemyGO.transform.position = transform.position;
    }
}
