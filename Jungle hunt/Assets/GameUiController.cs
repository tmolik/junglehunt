﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameUiController : MonoBehaviour
{

    public Image AirFill;
    public Image HpFill;
    public Text InfoText;
    internal PlayerController playerController;

    public Text PointsText;
    public Text LifePointsText;
    // Use this for initialization
    void Start()
    {
        /*Przypisanie player controller*/
        playerController = GameObject.Find("Player").GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        /*Jeśli to etap wodny to aktualizuj ilość dostępnego powietrza*/
        if (GameController.Get.CurrentGameMode == GameController.GameMode.Water)
        {
            if (AirFill != null && playerController!=null)
            AirFill.fillAmount = Mathf.Lerp(AirFill.fillAmount, playerController.airAvailable, Time.deltaTime * 4);
        }
        HpFill.fillAmount = Mathf.Lerp(HpFill.fillAmount, playerController.HP, Time.deltaTime * 4);

        PointsText.text = GameController.Get.Points.ToString();
        if (LifePointsText != null)
            LifePointsText.text = GameController.Get.LifePoints.ToString();
    }
    /*Aktualizowanie tekstu informacyjnego*/
    public void SetText(string text)
    {
        InfoText.text = text;
    }
}
