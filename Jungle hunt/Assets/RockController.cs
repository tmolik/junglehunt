﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RockController : MonoBehaviour {


    public Sprite RockSprite;
	// Use this for initialization
	void Start () {
        GetComponent<SpriteRenderer>().sprite = RockSprite;
        transform.localScale = new Vector3(0.6f, 0.6f, 1);
	}
	
	// Update is called once per frame
	void Update () {
        transform.position += Vector3.down * Time.deltaTime * 5;
        transform.Rotate(new Vector3(0, 0,1), 150 * Time.deltaTime);
        if (transform.position.y < -10)
            Destroy(gameObject);
	}

    public void OnTriggerEnter2D(Collider2D coll)
    {
        if(coll.gameObject.name == "Player")
        {
            coll.GetComponent<PlayerController>().HP -= 0.4f;
            GameController.Get.Points--;
            Destroy(gameObject);
        }
    }
}
