﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LianaController : MonoBehaviour
{

    private float timer;
    public int range = 15;
    public float speedMultiplier = 1f;
    public GameObject ChildPrefab;
    public bool isLast = false;
    public bool isFake = false;
    public bool isFalling = false;
    private GameObject lianaChild;
    // Use this for initialization
    void Start()
    {
        timer = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isFalling)
        {
            transform.rotation = Quaternion.Euler(0, 0, Mathf.Sin(timer * speedMultiplier) * range);
            timer += Time.deltaTime;

            if (lianaChild)
                lianaChild.transform.localPosition = new Vector3(0, lianaChild.transform.localPosition.y, 0);
        }
        /*Spadająca liana*/
        else
        {
            transform.position += Vector3.down * Time.deltaTime * 2f;
        }
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.name == "Player")
        {
            if (!coll.GetComponent<PlayerController>().onLiana)
            {
                GameObject child = Instantiate(ChildPrefab, transform);
                child.transform.localPosition = new Vector3(0, 0, 0);
                child.transform.position = new Vector3(child.transform.position.x, coll.transform.position.y, child.transform.position.z);

                coll.GetComponent<PlayerController>().Liana = gameObject;
                coll.GetComponent<PlayerController>().LianaChild = child;
                coll.GetComponent<PlayerController>().onLiana = true;
                coll.GetComponent<PlayerController>().jumping = false;
                coll.GetComponent<Animator>().Play("playerHold");
                
                GameController.Get.Points += 1;
                lianaChild = child;
                GetComponent<BoxCollider2D>().enabled = false;

                if (isFake)
                    isFalling = true;

                if (isLast)
                {
                    coll.GetComponent<AudioSource>().clip = GameController.Get.WinClip;
                    coll.GetComponent<AudioSource>().Play();
                    GameController.Get.changingMap = true;
                }
            }
        }
    }

}
