﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonkeyController : MonoBehaviour
{

    public GameObject RockPrefab;

    private float timer;
    private float timeToThrowARock;
    private bool throwing;
    // Use this for initialization
    void Start()
    {
        timer = 0;
        timeToThrowARock = Random.Range(1, 4);
        GetComponent<Animator>().Play("monkeyIdle");

    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (throwing)
        {
            if (timer >= timeToThrowARock + 0.3f)
            {
                timer = 0;
                throwing = false;
                ThrowRock();
                timeToThrowARock = Random.Range(2, 5);
                GetComponent<Animator>().Play("monkeyIdle");

            }

        }
        if (timer > timeToThrowARock)
        {
            throwing = true;
            GetComponent<Animator>().Play("monkeyAnim");
        }


    }

    public void ThrowRock()
    {
        GetComponent<Animator>().Play("monkeyAnim");
        GameObject rock = Instantiate(RockPrefab);
        rock.transform.position = transform.position;
    }
}
